# Target URL

<https://epogrebnyak.gitlab.io/observer/>

# Steps to reproduce

## Start project linked to Gitlab repository

```
# rm --rf .git
git init -b master
git add .
git commit -am"start project"
git push --set-upstream git@gitlab.com:epogrebnyak/observer.git master
```

The repo now tracks <https://gitlab.com:epogrebnyak/observer/>

## Add a theme

```
git submodule add https://github.com/sunt-programator/CodeIT.git themes/CodeIT
```

## Add cofig file

You will need a specific [config.toml][basic_config] to run this theme.

[basic_config]: https://codeit.suntprogramator.dev/theme-documentation-basics/#basic-configuration

## Tools to consider

- https://github.com/profclems/glab
