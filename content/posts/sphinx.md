---
title: "Buildng documentation with Sphinx"
date: 2019-05-17
draft: true
tags: ["haskell"]
---

## Comments

sphinx is extemely powerful and versatile, but why using [sphinx-doc][sphinx-doc] is hard?
Possible reasons:

- default setup is complex (tweaking sys.path for discoverability, many options not of use)
- two uses in one: rst source and api documentation, rst directives language + rst as markup 
- you can just easilty get lost in the docs
- [got to learn a lot before even starting](https://twitter.com/mistersql/status/1097238985792016384)
- old project, many old design desisions and documentation

## Themes

- https://github.com/pradyunsg/furo


## Blogs:

- https://predictablynoisy.com/posts/2020/sphinx-blogging


## Utilities:

- [flinx](https://github.com/osteele/flinx/blob/master/flinx/templates/conf.py.tpl) conf.py generator
- [pyproject.toml - > conf.py](https://github.com/epogrebnyak/weo-reader/blob/master/make_conf.py)

