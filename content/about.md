---
title: "About"
date: 2021-01-12T18:39:07+03:00
draft: false
---

## Themes

This site is created using [Hugo][hugo] static site generator
and [CodeIT][codeit] theme, a compelled fork from [LoveIt][loveit].

|        Theme         |                                     Stars                                     |                                     Forks                                     |
| :------------------: | :---------------------------------------------------------------------------: | :---------------------------------------------------------------------------: |
|   [LoveIt][loveit]   |     ![](https://img.shields.io/github/stars/dillonzq/LoveIt?style=social)     |     ![](https://img.shields.io/github/forks/dillonzq/LoveIt?style=social)     |
|   [CodeIT][codeit]   | ![](https://img.shields.io/github/stars/sunt-programator/CodeIT?style=social) | ![](https://img.shields.io/github/forks/sunt-programator/CodeIT?style=social) |
| [uBlogger][ublogger] |     ![](https://img.shields.io/github/stars/uPagge/uBlogger?style=social)     |     ![](https://img.shields.io/github/forks/uPagge/uBlogger?style=social)     |

{{< mermaid >}}
graph LR;
L(LeaveIt) --> K(KeepIt)
L --> V(LoveIt)
K --> V
V --> U(uBlogger)
V --> C(CodeIT)
{{< /mermaid >}}

![](https://raw.githubusercontent.com/dillonzq/LoveIt/master/images/Apple-Devices-Preview.png)

## Source

The source files for this blog are [hosted on Gitlab][repo-gl].

[hugo]: https://gohugo.io
[loveit]: https://hugoloveit.com
[repo-gl]: https://gitlab.com/epogrebnyak/observer
[repo]: https://github.com/epogrebnyak/icode
[codeit]: https://codeit.suntprogramator.dev/
[ublogger]: https://github.com/uPagge/uBlogger

## Images

Front matter uses a photo from [Hello I'm Nik](https://unsplash.com/photos/Yg0YZYIP8ak).
