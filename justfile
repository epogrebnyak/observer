serve:
    hugo server --disableFastRender

clean:
    rm -rf public resources

pretty:
    npx prettier -w content

publish:
    git add . && git commit -m"update" && git push

echo:         
    echo

rebase n:
    git rebase --interactive HEAD~{{n}} 